﻿/*

Copyright (c) 2020, CIMCON Automation
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#ifndef _P2P_IPC_CPP
#define _P2P_IPC_CPP

#include <string>
#include <list>

// Shared libary load/unload handlers
void __attribute__((constructor)) library_load();
void __attribute__((destructor)) library_unload();

// Structures and types
typedef enum MessageType
{
    Data=0,
    Event=1,
    Request=2,
    Response=3
}MessageType;

class IIPCCallback
{
    public:
        IIPCCallback() {}
        virtual ~IIPCCallback() {}
        virtual void OnNodeOnline(const std::string &nodename) = 0;
        virtual void OnNodeOffline(const std::string &nodename) = 0;
        virtual void OnData(const std::string &nodename, const std::string &messagebuffer) = 0;
        virtual void OnEvent(const std::string &nodename, const std::string &messagebuffer) = 0;
        virtual void OnRequest(const std::string &nodename, const std::string &messagebuffer) = 0;
        virtual void OnResponse(const std::string &nodename, const std::string &messagebuffer) = 0;
};

class IPCManager
{
    public:
        IPCManager();
        virtual ~IPCManager();
        // Session management
        bool Initialize();
        bool Open(IIPCCallback *callback);
        bool Close();

        // Node management
        bool Register();
        bool DeRegister();
        std::list<std::string>* GetPeerlist();

        // Messaging
        bool Send(const std::string &nodename, MessageType messagetype, const std::string &messagebuffer);        
};

#endif
