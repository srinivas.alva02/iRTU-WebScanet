#ifndef _IO_SERVICE_SIMULATOR
#define _IO_SERVICE_SIMULATOR

#include "P2PIPC.hpp"
#include "Configuration.hpp"

class IOServiceSimulator : public IIPCCallback
{
    public:
        IOServiceSimulator();
        virtual ~IOServiceSimulator();
        bool LoadConfiguration();
        bool Start();
        bool ReStart();
        bool Stop();
    protected:
        void OnNodeOnline(const std::string &nodename);
        void OnNodeOffline(const std::string &nodename);
        void OnData(const std::string &nodename, const std::string &messagebuffer);
        void OnEvent(const std::string &nodename, const std::string &messagebuffer);
        void OnRequest(const std::string &nodename, const std::string &messagebuffer);
        void OnResponse(const std::string &nodename, const std::string &messagebuffer);
    private:
        Configuration rs485_configuration;
        IPCManager ipc;
};

#endif
