#include "IOServiceSimulator.hpp"
#include "Logger.hpp"
#include "StringEx.hpp"
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <algorithm>
#include <thread>
#include <chrono>

static std::string GetTimeStampString();

static std::string payload_template = "{\"device_id\":\"000\", \"timestamp\":\"<timestamp>\", \"vibrationX\":\"<vibrationX>\", \"vibrationZ\":\"<vibrationZ>\", \"temperatureC\":\"<temperatureC>\"}";

IOServiceSimulator::IOServiceSimulator()
{

}

IOServiceSimulator::~IOServiceSimulator()
{

}

bool IOServiceSimulator::LoadConfiguration()
{
    // Read the service configuration file
    // Configuration file locate must conform to UNIX standards
    // /bin -> /etc
    // /usr/bin -> /usr/etc
    // /usr/local/bin -> /usr/local/etc

    //--> Configuration file

    // Auto locate and load the configuration
    // Take a big enough buffer to determine the current working directory
    char *curr_exec_dir = (char*)calloc(2049, 1);
    curr_exec_dir = getcwd(curr_exec_dir, 2048);
    // Strip off any sub-directories inside 'bin'
    size_t pos = 0;
    pos = (size_t)strstr(curr_exec_dir, "/bin");

    if(pos < 1)
    {
        for(int idx = strlen(curr_exec_dir)-1; curr_exec_dir[idx] != '/'; idx--)
        {
            curr_exec_dir[idx] = 0;
        }
    }
    else
    {
        for(int idx = pos; idx <= 2049; idx++)
        {
            curr_exec_dir[idx] = 0;
        }
    }

    // This is the valid location
    strcat(curr_exec_dir, "etc/IOServiceSimulator.conf");

    //--> Configuration search complete

    bool res = rs485_configuration.LoadConfiguration(std::string(curr_exec_dir));

    free(curr_exec_dir);

    if(!res)
    {
        return false;
    }

    return true;
}


bool IOServiceSimulator::Start()
{
    if(!ipc.Initialize())
    {
        writeLog("Could not intialize IPC", LOG_ERROR);
        return false;
    }

    if(!ipc.Open(this))
    {
        writeLog("Could not open IPC", LOG_ERROR);
        return false;
    }

    if(!ipc.Register())
    {
        writeLog("Could not register with the message bus", LOG_ERROR);
        return false;
    }

    int ctr = 0;

    while(true)
    {
        std::this_thread::sleep_for (std::chrono::seconds(10));

        std::string payload = payload_template;
        std::string str;

        std::string timestamp = GetTimeStampString();
        timestamp.resize(timestamp.size() - 1);
        strreplace(payload, "<timestamp>", timestamp);

        double temperatureC = (double)(ctr%20)+50;
        str.clear();
        strrealtostring(str, temperatureC);
        strreplace(payload, "<temperatureC>", str);

        double vibrationX = (double)(ctr%5)+2;
        str.clear();
        strrealtostring(str, vibrationX);
        strreplace(payload, "<vibrationX>", str);

        double vibrationZ = (double)(ctr%25)+2;
        str.clear();
        strrealtostring(str, vibrationZ);
        strreplace(payload, "<vibrationZ>", str);


        std::list<std::string> *plist = ipc.GetPeerlist();

        if(std::find(plist->begin(), plist->end(), "TransportService") != plist->end())
        {
            ipc.Send("TransportService", Data, payload);
        }

        ctr++;

        if(ctr > 100)
        {
            ctr++;
        }
    }

    return true;
}

bool IOServiceSimulator::ReStart()
{
    return false;
}

bool IOServiceSimulator::Stop()
{
    ipc.DeRegister();
    ipc.Close();
    return false;
}

void IOServiceSimulator::OnNodeOnline(const std::string &nodename)
{

}

void IOServiceSimulator::OnNodeOffline(const std::string &nodename)
{

}

void IOServiceSimulator::OnData(const std::string &nodename, const std::string &messagebuffer)
{

}

void IOServiceSimulator::OnEvent(const std::string &nodename, const std::string &messagebuffer)
{

}

void IOServiceSimulator::OnRequest(const std::string &nodename, const std::string &messagebuffer)
{

}

void IOServiceSimulator::OnResponse(const std::string &nodename, const std::string &messagebuffer)
{

}

std::string GetTimeStampString()
{
    struct tm timeinfo;
    time_t rawtime;
    time(&rawtime);
    timeinfo = *localtime(&rawtime);

    std::string str = "yyyyMMddhhmmss";
    size_t pos = 0;
    bool ap = false;

    char buffer[256];
    memset((char*)&buffer[0], 0, 256);

    pos = str.find("ss");
    if (pos != -1)
    {
        strreplace(str, "ss", "%S");
    }

    pos = str.find("mm");
    if (pos != -1)
    {
        strreplace(str, "mm", "%M");
    }

    pos = str.find("hh");
    if (pos != -1)
    {
        strreplace(str, "hh", "%H");
    }
    else
    {
        pos = str.find("h");
        if (pos != -1)
        {
            strreplace(str, "h", "%I");
            ap = true;
        }
    }

    pos = str.find("dd");
    if (pos != -1)
    {
        strreplace(str, "dd", "%d");
    }

    pos = str.find("MMMM");
    if (pos != -1)
    {
        strreplace(str, "MMMM", "%B");
    }
    else
    {
        pos = str.find("MM");
        if (pos != -1)
        {
            strreplace(str, "MM", "%m");
        }
    }

    pos = str.find("yyyy");
    if (pos != -1)
    {
        strreplace(str, "yyyy", "%Y");
    }
    else
    {
        pos = str.find("yy");
        if (pos != -1)
        {
            strreplace(str, "yy", "%y");
        }
    }

    if (ap)
    {
        str += "%p";

    }

    if (timeinfo.tm_year < 100)
    {
        timeinfo.tm_year += 100;
    }

    strftime(buffer, 256, str.c_str(), &timeinfo);

    return buffer;
}

