#include "IOServiceSimulator.hpp"
#include "Logger.hpp"


int main(int argc, char* argv[])
{
    IOServiceSimulator service;
    Logger* service_logger = Logger::GetInstance();

    service_logger->SetModuleName("IOServiceSimulator");
    service_logger->StartLogging();

    if(!service.LoadConfiguration())
    {
        writeLog("Could not load configuration", LOG_ERROR);

        return -1;
    }

    writeLog("Loaded service configuration", LOG_INFO);

    writeLog("Starting service", LOG_INFO);

    service.Start();

    writeLog("Exiting", LOG_INFO);

    service_logger->StopLogging();

    return 0;
}
