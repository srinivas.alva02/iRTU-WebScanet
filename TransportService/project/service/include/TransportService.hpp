#ifndef _TRANSPORT_SERVICE
#define _TRANSPORT_SERVICE

#include "P2PIPC.hpp"
#include "IProtocolINET.hpp"
#include "Configuration.hpp"

class TransportService : public IIPCCallback
{
    public:
        TransportService();
        virtual ~TransportService();
        bool LoadConfiguration();
        bool Start();
        bool ReStart();
        bool Stop();
    protected:
        void OnNodeOnline(const std::string &nodename);
        void OnNodeOffline(const std::string &nodename);
        void OnData(const std::string &nodename, const std::string &messagebuffer);
        void OnEvent(const std::string &nodename, const std::string &messagebuffer);
        void OnRequest(const std::string &nodename, const std::string &messagebuffer);
        void OnResponse(const std::string &nodename, const std::string &messagebuffer);
    private:
        Configuration transport_configuration;
        IPCManager ipc;
        std::string protocol_name;
        std::string server_name;
        int server_port;
        std::string server_uri;
        std::string device_id;
        bool enabled;
        IProtocolINET *inet_protocol;
};

#endif
