#include "TransportService.hpp"
#include "Logger.hpp"
#include "StringEx.hpp"
#include "ProtocolHTTP.hpp"
#include "ProtocolMQTT.hpp"
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <algorithm>
#include <thread>
#include <chrono>
#include <iostream>

static std::string GetTimeStampString();

static std::string payload_template = "{\"encoding\":\"utf-8\",\"transport_type\":\"<transport_type>\",\"rtuid\":\"<rtuid>\",\"application_id\":\"<application_id>\",\"version\":\"1.0.0\",\"message_type\":\"telemetry\",\"latitude\":\"23.00\",\"longitude\":\"72.00\",\"timestamp\":\"<timestamp>\", \"payload\":[<payload>]}";

TransportService::TransportService()
{
    inet_protocol = nullptr;
}

TransportService::~TransportService()
{

}

bool TransportService::LoadConfiguration()
{
    // Read the service configuration file
    // Configuration file locate must conform to UNIX standards
    // /bin -> /etc
    // /usr/bin -> /usr/etc
    // /usr/local/bin -> /usr/local/etc

    //--> Configuration file

    // Auto locate and load the configuration
    // Take a big enough buffer to determine the current working directory
    char *curr_exec_dir = (char*)calloc(2049, 1);
    curr_exec_dir = getcwd(curr_exec_dir, 2048);
    // Strip off any sub-directories inside 'bin'
    size_t pos = 0;
    pos = (size_t)strstr(curr_exec_dir, "/bin");

    if(pos < 1)
    {
        for(int idx = strlen(curr_exec_dir)-1; curr_exec_dir[idx] != '/'; idx--)
        {
            curr_exec_dir[idx] = 0;
        }
    }
    else
    {
        for(int idx = pos; idx <= 2049; idx++)
        {
            curr_exec_dir[idx] = 0;
        }
    }

    // This is the valid location
    strcat(curr_exec_dir, "etc/TransportService.conf");

    //--> Configuration search complete

    bool res = transport_configuration.LoadConfiguration(std::string(curr_exec_dir));

    free(curr_exec_dir);

    if(!res)
    {
        return false;
    }

    server_name = transport_configuration.GetValue("General", "server", "127.0.0.1");
    server_port = atoi(transport_configuration.GetValue("General", "port", "1883").c_str());
    protocol_name = transport_configuration.GetValue("General", "protocol", "mqtt");
    server_uri = transport_configuration.GetValue("General", "uri", "irtu");
    device_id = transport_configuration.GetValue("General", "device_id", "000");
    enabled = (bool)atoi(transport_configuration.GetValue("General", "enabled", "1").c_str());

    return true;
}

bool TransportService::Start()
{
    if(enabled)
    {
        if(protocol_name == "http")
        {
            inet_protocol = new ProtocolHTTP();
        }
        else
        {
            if(protocol_name == "mqtt")
            {
                inet_protocol = new ProtocolMQTT();
            }
            else
            {
                return false;
            }
        }

        if(!inet_protocol->Initialize(server_port, server_name))
        {
            return false;
        }

        if(!inet_protocol->Connect())
        {
            return false;
        }
    }

    if(!ipc.Initialize())
    {
        writeLog("Could not intialize IPC", LOG_ERROR);
        return false;
    }

    if(!ipc.Open(this))
    {
        writeLog("Could not open IPC", LOG_ERROR);
        return false;
    }

    if(!ipc.Register())
    {
        writeLog("Could not register with the message bus", LOG_ERROR);
        return false;
    }

    while(true)
    {
        std::this_thread::sleep_for (std::chrono::seconds(10));
    }

    return true;
}

bool TransportService::ReStart()
{
    return false;
}

bool TransportService::Stop()
{
    ipc.DeRegister();
    ipc.Close();
    return false;
}

void TransportService::OnNodeOnline(const std::string &nodename)
{

}

void TransportService::OnNodeOffline(const std::string &nodename)
{

}

void TransportService::OnData(const std::string &nodename, const std::string &messagebuffer)
{
    if(inet_protocol)
    {
        std::string payload = payload_template;

        std::string ts = GetTimeStampString();

        strreplace(payload, "<transport_type>", protocol_name);
        strreplace(payload, "<rtuid>", device_id);
        strreplace(payload, "<application_id>", nodename);
        strreplace(payload, "<timestamp>", ts);
        strreplace(payload, "<payload>", messagebuffer);

        inet_protocol->SendPayload(payload, server_uri);

        //std::cout << payload << std::endl;
    }
}

void TransportService::OnEvent(const std::string &nodename, const std::string &messagebuffer)
{

}

void TransportService::OnRequest(const std::string &nodename, const std::string &messagebuffer)
{

}

void TransportService::OnResponse(const std::string &nodename, const std::string &messagebuffer)
{

}

std::string GetTimeStampString()
{
    struct tm timeinfo;
    time_t rawtime;
    time(&rawtime);
    timeinfo = *localtime(&rawtime);

    std::string str = "yyyyMMddhhmmss";
    size_t pos = 0;
    bool ap = false;

    char buffer[256];
    memset((char*)&buffer[0], 0, 256);

    pos = str.find("ss");
    if (pos != -1)
    {
        strreplace(str, "ss", "%S");
    }

    pos = str.find("mm");
    if (pos != -1)
    {
        strreplace(str, "mm", "%M");
    }

    pos = str.find("hh");
    if (pos != -1)
    {
        strreplace(str, "hh", "%H");
    }
    else
    {
        pos = str.find("h");
        if (pos != -1)
        {
            strreplace(str, "h", "%I");
            ap = true;
        }
    }

    pos = str.find("dd");
    if (pos != -1)
    {
        strreplace(str, "dd", "%d");
    }

    pos = str.find("MMMM");
    if (pos != -1)
    {
        strreplace(str, "MMMM", "%B");
    }
    else
    {
        pos = str.find("MM");
        if (pos != -1)
        {
            strreplace(str, "MM", "%m");
        }
    }

    pos = str.find("yyyy");
    if (pos != -1)
    {
        strreplace(str, "yyyy", "%Y");
    }
    else
    {
        pos = str.find("yy");
        if (pos != -1)
        {
            strreplace(str, "yy", "%y");
        }
    }

    if (ap)
    {
        str += "%p";

    }

    if (timeinfo.tm_year < 100)
    {
        timeinfo.tm_year += 100;
    }

    strftime(buffer, 256, str.c_str(), &timeinfo);

    return buffer;
}


