#include "TransportService.hpp"
#include "Logger.hpp"


int main(int argc, char* argv[])
{
    TransportService service;
    Logger* service_logger = Logger::GetInstance();

    service_logger->SetModuleName("TransportService");
    service_logger->StartLogging();

    if(!service.LoadConfiguration())
    {
        writeLog("Could not load configuration", LOG_ERROR);

        return -1;
    }

    service.Start();

    return 0;
}
