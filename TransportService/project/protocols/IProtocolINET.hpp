#ifndef _I_PROTOCOL_INET
#define _I_PROTOCOL_INET

#include <string>

class IProtocolINET
{
    public:
        IProtocolINET() {}
        virtual ~IProtocolINET() {}
        virtual bool Initialize(int p, const std::string &srv) = 0;
        virtual bool Connect() = 0;
        virtual bool SendPayload(const std::string &str, const std::string &uri) = 0;
    protected:
       std::string server;
       int port;
};

#endif
