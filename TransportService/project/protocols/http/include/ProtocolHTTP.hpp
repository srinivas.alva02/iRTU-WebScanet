#ifndef PROTOCOL_HTTP
#define PROTOCOL_HTTP

#include "IProtocolINET.hpp"

class ProtocolHTTP : public IProtocolINET
{
    public:
        ProtocolHTTP();
        virtual ~ProtocolHTTP();
        virtual bool Initialize(int p, const std::string &srv);
        virtual bool Connect();
        virtual bool SendPayload(const std::string &str, const std::string &uri);
};


#endif
