#ifndef PROTOCOL_MQTT
#define PROTOCOL_MQTT

#include "IProtocolINET.hpp"

class ProtocolMQTT : public IProtocolINET
{
    public:
        ProtocolMQTT();
        virtual ~ProtocolMQTT();
        virtual bool Initialize(int p, const std::string &srv);
        virtual bool Connect();
        virtual bool SendPayload(const std::string &str, const std::string &uri);
};

#endif

