#include "ProtocolMQTT.hpp"
#include <stdio.h>
#include <mosquitto.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static struct mosquitto *mosq = nullptr;
static int keepalive = 60*12*60;

ProtocolMQTT::ProtocolMQTT()
{
}

ProtocolMQTT::~ProtocolMQTT()
{
}

bool ProtocolMQTT::Initialize(int p, const std::string &srv)
{
    port = p;
    server = srv;

    bool clean_session = true;

    mosquitto_lib_init();

    mosq = mosquitto_new(NULL, clean_session, NULL);

    if(!mosq)
    {
        return false;
    }

    return true;
}

bool ProtocolMQTT::Connect()
{
    if(mosquitto_connect(mosq, server.c_str(), port, keepalive) != MOSQ_ERR_SUCCESS)
    {
        return false;
    }

    int loop = mosquitto_loop_start(mosq);

    if(loop != MOSQ_ERR_SUCCESS)
    {
        return false;
    }

    return true;
}

bool ProtocolMQTT::SendPayload(const std::string &str, const std::string &uri)
{
    std::string topic = uri + "/";
    if(mosquitto_publish(mosq, NULL, topic.c_str(), str.length(), str.c_str(), 0, 0) != MOSQ_ERR_SUCCESS)
    {
        return false;
    }

    return true;
}

