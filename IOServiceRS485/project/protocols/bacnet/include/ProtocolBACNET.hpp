#ifndef _PROTOCOL_BACNET
#define _PROTOCOL_BACNET

#include "IProtocolRS485.hpp"

class ProtocolBacnet : public IProtocolRS485
{
public:
    ProtocolBacnet();
    virtual ~ProtocolBacnet();
    bool SetDeviceName(const std::string &device_name, int slaveid);
    bool SetDeviceConfiguration(int bd, char pr, int sb, int db, int xo);
    bool Initialize();
    bool Read(unsigned int index, void** buffer);
    bool Write(unsigned int index, void* buffer);
};

#endif
