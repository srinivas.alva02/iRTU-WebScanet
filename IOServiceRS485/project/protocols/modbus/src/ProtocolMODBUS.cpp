#include "ProtocolMODBUS.hpp"
#include "Interface.hpp"
#include <string.h>

ProtocolModbus::ProtocolModbus()
{
    pModbus = new modbusInterface;
}

ProtocolModbus::~ProtocolModbus()
{
    delete pModbus;
}

bool ProtocolModbus::SetDeviceName(const std::string &device_name, int slaveid)
{
    strcpy(pModbus->mSerialPortCnfg.mPortName, device_name.c_str());
    pModbus->modbusCnfg.mSlaveId = slaveid;
    return false;
}

bool ProtocolModbus::SetDeviceConfiguration(int bd, char pr, int sb, int db, int xo)
{
    pModbus->mSerialPortCnfg.mBaudRate = bd;
    pModbus->mSerialPortCnfg.mParity = pr;
    pModbus->mSerialPortCnfg.mDataBits = db;
    pModbus->mSerialPortCnfg.mStopBits = sb;
    return true;
}

bool ProtocolModbus::Initialize()
{
    pModbus->modbus_init();
    pModbus->SetConfigurations();
    return true;
}

bool ProtocolModbus::Read(unsigned int index, void** buffer)
{
    pModbus->modbus_master_read_Data(index, buffer);
    return true;
}

bool ProtocolModbus::Write(unsigned int index, void* buffer)
{
    return false;
}

int ProtocolModbus::GetQueryCount()
{
    pModbus->GetNumberOfQuery();
}



