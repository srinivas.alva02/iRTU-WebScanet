#ifndef _PROTOCOL_MODBUS
#define _PROTOCOL_MODBUS

#include "IProtocolRS485.hpp"

class ProtocolModbus : public IProtocolRS485
{
public:
    ProtocolModbus();
    virtual ~ProtocolModbus();
    bool SetDeviceName(const std::string &device_name, int slaveid);
    bool SetDeviceConfiguration(int bd, char pr, int sb, int db, int xo);
    bool Initialize();
    bool Read(unsigned int index, void** buffer);
    bool Write(unsigned int index, void* buffer);
    int GetQueryCount();
};

#endif
