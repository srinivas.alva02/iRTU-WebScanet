﻿/*

Copyright (c) 2020, CIMCON Automation
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#include "P2PIPC.hpp"
#include "StringEx.hpp"
#include "Responder.hpp"
#include "Payload.hpp"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <fstream>
#include <string>
#include <map>
#include <vector>
#include <algorithm>
#include <thread>

static char message_bus_host[32] = {0};
static char process_name[32] = {0};
static int port = 49151;
static std::map<MessageType, std::string> message_topics;

static IIPCCallback* callback_ptr = nullptr;
static IPCManager* ipcmanager_ptr = nullptr;
static std::list<std::string> peerNodes;

static Responder responder;

static void read_current_process_name();
static bool ProcessEvents();

void HandleProtocol(Payload* message);

void __attribute__((constructor)) library_load()
{

}

void __attribute__((destructor)) library_unload()
{

}

IPCManager::IPCManager()
{
    ipcmanager_ptr = this;
}

IPCManager::~IPCManager()
{

}

bool IPCManager::Initialize()
{
    strcpy(message_bus_host, "localhost");
    port = 49151;

    read_current_process_name();

    // Topic name mapping

    message_topics[Data]       ="DATA";
    message_topics[Event]    ="EVENT";
    message_topics[Request]     ="REQUEST";
    message_topics[Response]    ="RESPONSE";

    // Topic name mapping complete

    if(!responder.createSocket(message_bus_host, port))
    {
        return false;
    }

    return true;
}

bool IPCManager::Open(IIPCCallback *callback)
{
    int ret_code;

    if(!responder.connectSocket(ret_code))
    {
        return false;
    }

    callback_ptr = callback;

    std::thread listprocthread(ProcessEvents);
    listprocthread.detach();

    return true;
}

bool IPCManager::Close()
{
    return responder.closeSocket();
}

// Node management
bool IPCManager::Register()
{
    Payload reg_payload;

    reg_payload.setProtocolInformation("REGISTER", "/", "MESSAGEBUS", "1.0.0");
    reg_payload.addHeader("Sender", process_name);
    reg_payload.addHeader("Recipient", "MessageBus");

    std::string data;
    reg_payload.serialize(data);
    int len = data.length();

    return responder.sendBuffer(data.c_str(), len);
}

bool IPCManager::DeRegister()
{ 
    Payload dereg_payload;

    dereg_payload.setProtocolInformation("DEREGISTER", "/", "MESSAGEBUS", "1.0.0");
    dereg_payload.addHeader("Sender", process_name);
    dereg_payload.addHeader("Recipient", "MessageBus");

    std::string data;
    dereg_payload.serialize(data);
    int len = data.length();

    return responder.sendBuffer(data.c_str(), len);
}

std::list<std::string> *IPCManager::GetPeerlist()
{
    return &peerNodes;
}

// Messaging
bool IPCManager::Send(const std::string &nodename, MessageType messagetype, const std::string &messagebuffer)
{
    Payload reg_payload;
    int len = 0;

    std::string qualified_topic = message_topics[messagetype];

    char content_length[8] = {0};
    sprintf(content_length, "%d", messagebuffer.length());

    reg_payload.setProtocolInformation(qualified_topic.c_str(), "/", "MESSAGEBUS", "1.0.0");
    reg_payload.addHeader("Sender", process_name);
    reg_payload.addHeader("Recipient", nodename.c_str());
    reg_payload.addHeader("Content-Length", content_length);
    reg_payload.attachBody(messagebuffer.c_str());

    std::string searialized_header;
    reg_payload.serialize(searialized_header);

    len = searialized_header.length();

    if(!responder.sendBuffer(searialized_header.c_str(), len))
    {
        return false;
    }

    len = messagebuffer.length();

    if(!responder.sendBuffer(messagebuffer.c_str(), len))
    {
        return false;
    }

    return true;
}

bool ProcessEvents()
{
    if(!responder.isConnected())
    {
        return false;
    }

    while(true)
    {
        std::string header;
        Payload message;
        if(responder.receiveString(header,(char*)"\r\n\r\n"))
        {
            message.setHeader(header.c_str());
            message.deSerialize();

            if(message.hasBody())
            {
                char *buffer = nullptr;

                int len = message.getContentSize();

                buffer = new char[len+1];
                memset(buffer,0,len+1);

                if(responder.receiveBuffer(buffer,len))
                {
                    message.attachBody(buffer);
                    delete [] buffer;
                    buffer = nullptr;
                }
                else
                {
                    break;
                }
            }

            HandleProtocol(&message);
        }
        else
        {
            break;
        }
    }

    return true;
}

void HandleProtocol(Payload* message)
{
    std::string sender = message->getHeader("Sender");

    // We get this once we connect and regsiter ourselves
    if(strcmp((const char*)message->getRequest(),"NODELIST")==0)
    {
        std::list<std::string> nodes;
        strsplit(message->getContent(), nodes, ',', true);
        std::copy(peerNodes.begin(), peerNodes.end(), nodes.begin());
    }

    // We get this when there is a REGSITER at the server except ours own
    if(strcmp((const char*)message->getRequest(),"NODEONLINE")==0)
    {
        std::string node = message->getHeader("Node");
        if(std::find(peerNodes.begin(), peerNodes.end(), node) == peerNodes.end())
        {
            peerNodes.push_back(node);
            callback_ptr->OnNodeOnline(node);
        }
    }

    // We get this when there is a DEREGSITER at the server
    if(strcmp((const char*)message->getRequest(),"NODEOFFLINE")==0)
    {
        std::string node = message->getHeader("Node");
        if(std::find(peerNodes.begin(), peerNodes.end(), node) != peerNodes.end())
        {
            peerNodes.remove(node);
            callback_ptr->OnNodeOffline(node);
        }
    }

    if(strcmp((const char*)message->getRequest(),"DATA")==0)
    {
        callback_ptr->OnData(sender, message->getContent());
    }

    if(strcmp((const char*)message->getRequest(),"EVENT")==0)
    {
        callback_ptr->OnEvent(sender, message->getContent());
    }

    if(strcmp((const char*)message->getRequest(),"REQUEST")==0)
    {
        callback_ptr->OnRequest(sender, message->getContent());
    }

    if(strcmp((const char*)message->getRequest(),"RESPONSE")==0)
    {
        callback_ptr->OnResponse(sender, message->getContent());
    }
}

void read_current_process_name()
{
    //Get the process name

    char procfs_path[1024] = {0};
    pid_t pid = getpid();

    sprintf(procfs_path, "/proc/%d/cmdline", pid);

    std::ifstream file;
    std::string line;

    file.open(procfs_path);

    if (!file.good())
    {
        return;
    }

    line.clear();
    std::getline(file, line);

    std::vector<std::string> cmd_args;
    strsplit(line, cmd_args, ' ', true);

    std::vector<std::string> dir_toks;
    strsplit(cmd_args[0], dir_toks, '/', true);
    strcpy(process_name, dir_toks[dir_toks.size()-1].c_str());

    file.close();

    // Prcoess determined
}
