#ifndef _IO_RS485_SERVICE
#define _IO_RS485_SERVICE

#include "P2PIPC.hpp"
#include "IProtocolRS485.hpp"
#include "Configuration.hpp"

class IOServiceRS485 : public IIPCCallback
{
    public:
        IOServiceRS485();
        virtual ~IOServiceRS485();
        bool LoadConfiguration();
        bool Start();
        bool ReStart();
        bool Stop();
    protected:
        void OnNodeOnline(const std::string &nodename);
        void OnNodeOffline(const std::string &nodename);
        void OnData(const std::string &nodename, const std::string &messagebuffer);
        void OnEvent(const std::string &nodename, const std::string &messagebuffer);
        void OnRequest(const std::string &nodename, const std::string &messagebuffer);
        void OnResponse(const std::string &nodename, const std::string &messagebuffer);
    private:
        Configuration rs485_configuration;
        IPCManager ipc;
        IProtocolRS485 *io_protocol;

        bool enabled;
        std::string device_id;
        std::string protocol_name;
        std::string device;
        int baud_rate;
        char parity;
        int data_bits;
        int stop_bits;
        int xon_off;
};

#endif
