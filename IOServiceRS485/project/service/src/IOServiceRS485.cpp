#include "IOServiceRS485.hpp"
#include "ProtocolMODBUS.hpp"
#include "ProtocolBACNET.hpp"
#include "Logger.hpp"
#include "StringEx.hpp"

#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <algorithm>
#include <thread>
#include <chrono>
#include <iostream>

static std::string GetTimeStampString();

static std::string payload_template = "{\"device_id\":\"000\", \"timestamp\":\"<timestamp>\", \"vibrationX\":\"<vibrationX>\", \"vibrationZ\":\"<vibrationZ>\", \"temperatureC\":\"<temperatureC>\"}";


IOServiceRS485::IOServiceRS485()
{

}

IOServiceRS485::~IOServiceRS485()
{

}

bool IOServiceRS485::LoadConfiguration()
{
    // Read the service configuration file
    // Configuration file locate must conform to UNIX standards
    // /bin -> /etc
    // /usr/bin -> /usr/etc
    // /usr/local/bin -> /usr/local/etc

    //--> Configuration file

    // Auto locate and load the configuration
    // Take a big enough buffer to determine the current working directory
    char *curr_exec_dir = (char*)calloc(2049, 1);
    curr_exec_dir = getcwd(curr_exec_dir, 2048);
    // Strip off any sub-directories inside 'bin'
    size_t pos = 0;
    pos = (size_t)strstr(curr_exec_dir, "/bin");

    if(pos < 1)
    {
        for(int idx = strlen(curr_exec_dir)-1; curr_exec_dir[idx] != '/'; idx--)
        {
            curr_exec_dir[idx] = 0;
        }
    }
    else
    {
        for(int idx = pos; idx <= 2049; idx++)
        {
            curr_exec_dir[idx] = 0;
        }
    }

    // This is the valid location
    strcat(curr_exec_dir, "etc/IOServiceRS485.conf");

    //--> Configuration search complete

    bool res = rs485_configuration.LoadConfiguration(std::string(curr_exec_dir));

    free(curr_exec_dir);

    if(!res)
    {
        return false;
    }

    enabled = (bool)atoi(rs485_configuration.GetValue("General", "enabled", "1").c_str());
    device_id = rs485_configuration.GetValue("General", "device_id", "000");
    protocol_name = rs485_configuration.GetValue("General", "protocol", "modbus");
    device = rs485_configuration.GetValue("General", "device", "/dev/tty01");
    baud_rate = atoi(rs485_configuration.GetValue("General", "baud_rate", "115200").c_str());

    parity = rs485_configuration.GetValue("General", "parity", "N").c_str()[0];
    data_bits = atoi(rs485_configuration.GetValue("General", "data_bits", "8").c_str());
    stop_bits = atoi(rs485_configuration.GetValue("General", "stop_bits", "1").c_str());

    xon_off = atoi(rs485_configuration.GetValue("General", "xon_off", "0").c_str());

    return true;
}


bool IOServiceRS485::Start()
{
    if(enabled)
    {
        if(protocol_name == "modbus")
        {
            io_protocol = new ProtocolModbus();
        }
        else
        {
            if(protocol_name == "bacnet")
            {
                io_protocol = new ProtocolBacnet();
            }
        }
    }

    if(!ipc.Initialize())
    {
        writeLog("Could not intialize IPC", LOG_ERROR);
        return false;
    }

    if(!ipc.Open(this))
    {
        writeLog("Could not open IPC", LOG_ERROR);
        return false;
    }

    if(!ipc.Register())
    {
        writeLog("Could not register with the message bus", LOG_ERROR);
        return false;
    }

    io_protocol->SetDeviceName(device, 1);
    io_protocol->SetDeviceConfiguration(baud_rate, parity, stop_bits, data_bits, xon_off);
    io_protocol->Initialize();

    while(true)
    {
        std::this_thread::sleep_for (std::chrono::seconds(10));

        char *buffer = nullptr;
        float temperatureC = 0;
        float vibrationZ = 0;
        float vibrationX = 0;

        for (unsigned int idx = 0; idx < ((ProtocolModbus*)io_protocol)->GetQueryCount(); ++idx)
        {
            io_protocol->Read(idx, (void**)&buffer);

            if(idx == 0)
            {
                temperatureC = ((float)((buffer[1] << 8 ) | buffer[0])) / 100;
                printf("Temperature:%f Celcius\n", temperatureC);
            }

            if(idx == 2)
            {
                vibrationZ = ((float)((buffer[9] << 8 ) | buffer[8])) / 10;
                printf("Z-Axis Peak Velocity Component Frequency (Hz):%f\n", vibrationZ);
            }

            if(idx == 3)
            {
                vibrationX = ((float)((buffer[9] << 8 ) | buffer[8])) / 10;
                printf("X-Axis Peak Velocity Component Frequency (Hz):%f\n", vibrationX);
            }

            free(buffer);

            buffer = nullptr;
        }

        std::string payload = payload_template;
        std::string str;

        std::string timestamp = GetTimeStampString();
        timestamp.resize(timestamp.size() - 1);
        strreplace(payload, "<timestamp>", timestamp);

        str.clear();
        strrealtostring(str, temperatureC);
        strreplace(payload, "<temperatureC>", str);

        str.clear();
        strrealtostring(str, vibrationX);
        strreplace(payload, "<vibrationX>", str);

        str.clear();
        strrealtostring(str, vibrationZ);
        strreplace(payload, "<vibrationZ>", str);

        std::list<std::string> *plist = ipc.GetPeerlist();

        if(std::find(plist->begin(), plist->end(), "TransportService") != plist->end())
        {
            std::cout << payload << std::endl;
            ipc.Send("TransportService", Data, payload);
        }
    }

    return true;
}

bool IOServiceRS485::ReStart()
{
    return false;
}

bool IOServiceRS485::Stop()
{
    ipc.DeRegister();
    ipc.Close();
    return false;
}

void IOServiceRS485::OnNodeOnline(const std::string &nodename)
{

}

void IOServiceRS485::OnNodeOffline(const std::string &nodename)
{

}

void IOServiceRS485::OnData(const std::string &nodename, const std::string &messagebuffer)
{

}

void IOServiceRS485::OnEvent(const std::string &nodename, const std::string &messagebuffer)
{

}

void IOServiceRS485::OnRequest(const std::string &nodename, const std::string &messagebuffer)
{

}

void IOServiceRS485::OnResponse(const std::string &nodename, const std::string &messagebuffer)
{

}

std::string GetTimeStampString()
{
    struct tm timeinfo;
    time_t rawtime;
    time(&rawtime);
    timeinfo = *localtime(&rawtime);

    std::string str = "yyyyMMddhhmmss";
    size_t pos = 0;
    bool ap = false;

    char buffer[256];
    memset((char*)&buffer[0], 0, 256);

    pos = str.find("ss");
    if (pos != -1)
    {
        strreplace(str, "ss", "%S");
    }

    pos = str.find("mm");
    if (pos != -1)
    {
        strreplace(str, "mm", "%M");
    }

    pos = str.find("hh");
    if (pos != -1)
    {
        strreplace(str, "hh", "%H");
    }
    else
    {
        pos = str.find("h");
        if (pos != -1)
        {
            strreplace(str, "h", "%I");
            ap = true;
        }
    }

    pos = str.find("dd");
    if (pos != -1)
    {
        strreplace(str, "dd", "%d");
    }

    pos = str.find("MMMM");
    if (pos != -1)
    {
        strreplace(str, "MMMM", "%B");
    }
    else
    {
        pos = str.find("MM");
        if (pos != -1)
        {
            strreplace(str, "MM", "%m");
        }
    }

    pos = str.find("yyyy");
    if (pos != -1)
    {
        strreplace(str, "yyyy", "%Y");
    }
    else
    {
        pos = str.find("yy");
        if (pos != -1)
        {
            strreplace(str, "yy", "%y");
        }
    }

    if (ap)
    {
        str += "%p";

    }

    if (timeinfo.tm_year < 100)
    {
        timeinfo.tm_year += 100;
    }

    strftime(buffer, 256, str.c_str(), &timeinfo);

    return buffer;
}

