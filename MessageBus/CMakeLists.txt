cmake_minimum_required(VERSION 3.5)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

set(Project MessageBus)
project(${Project})

add_subdirectory(MessageBus)
add_subdirectory(libMessageBus)
add_subdirectory(examples) 
