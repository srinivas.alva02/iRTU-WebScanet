#include "Logger.hpp"
#include "StringEx.hpp"
#include <memory.h>
#include <unistd.h>
#include <stdarg.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdlib.h>

std::string getDateString(const char *format);
bool dirisdirectory(const std::string& str);
void dircurrentdirectory(std::string& str);
void dirgetname(const std::string& fullpath, std::string& str);
void dircreatedirectory(const std::string& str);
void dirgetparentdirectory(const std::string& str, std::string& pstr);

static Logger objLogger;

Logger*  Logger::GetInstance()
{
    return &objLogger;
}

Logger::Logger()
{
    _LogDirectory = "";
    _LogFileSize = 1024;
    _LogFile = nullptr;

    char pidstr[16];
    memset((char*)&pidstr[0],0,16);
    sprintf(pidstr,"%d",getpid());
    _ModuleName = pidstr;

    _LogLevelMap.clear();

    _LogLevelMap[LOG_INFO]       ="Information";
    _LogLevelMap[LOG_WARNING]    ="Warning    ";
    _LogLevelMap[LOG_ERROR]      ="Error      ";
    _LogLevelMap[LOG_CRITICAL]   ="Critical   ";
    _LogLevelMap[LOG_PANIC]      ="Panic      ";
}

Logger::~Logger()
{
    stopLogging();
}

void Logger::stopLogging()
{
    if(_LogFile != nullptr)
    {
        fflush(_LogFile);
        fclose(_LogFile);
    }
    _LogLevelMap.clear();
}

void Logger::createBackupFileName(std::string &str)
{
    std::string tstamp = getDateString("yyyy.MM.dd-hh.mm.ss");
    char temp[1024];
    memset((char*)&temp[0],0,16);
    sprintf(temp,"%s_%s.log",_ModuleName.c_str(),tstamp.c_str());
    str = temp;
}

void Logger::startLogging()
{
    if(_LogDirectory.empty() || _LogDirectory.length()<1)
    {
		std::string parent_dir, current_dir;
		dircurrentdirectory(current_dir);
		dirgetparentdirectory(current_dir, parent_dir);

		_LogDirectory = parent_dir + "/log/";

        if(!dirisdirectory(_LogDirectory))
        {
            dircreatedirectory(_LogDirectory);
        }
    }

    _LogFilename = _LogDirectory + _ModuleName + ".log";

    _LogFile = fopen(_LogFilename.c_str(),"w+");
}

void Logger::write(std::string logEntry, LogLevel llevel, const char* func, const char* file, int line)
{
    if(_LogFile != nullptr)
    {
        long sz = ftell(_LogFile);

        if(sz >= _LogFileSize*1024)
        {
			std::string temp;
            createBackupFileName(temp);
			std::string backupfile = _LogBackupDirectory + temp;
            stopLogging();
            rename(_LogFilename.c_str(),backupfile.c_str());
            startLogging();
        }

		std::string sourcefile;
		dirgetname(file, sourcefile);
		std::string lvel = _LogLevelMap[llevel];

        std::string tstamp = getDateString("yyyy.MM.dd-hh.mm.ss");
        char temp[1024];
        memset((char*)&temp[0],0,16);

        char fname[256]={0};
        memcpy(fname,func,255);
        #if defined(_WIN32) || defined(WIN32)
        #else
        int pos = strcharacterpos(fname,'(');
        fname[pos]=0;
        #endif

		std::string left, right;

        strsplit(fname, "::", left, right);
        if(right.length()>1)
        {
            strcpy(fname,right.c_str());
        }

        strsplit(fname, " ", left, right);
        if(right.length()>1)
        {
            strcpy(fname,right.c_str());
        }

        sprintf(temp,"%s|%s|%05d|%s|%s| ",tstamp.c_str(),lvel.c_str(),line,fname,sourcefile.c_str());

        logEntry = temp + logEntry;
        fprintf(_LogFile,"%s\n",logEntry.c_str());
        fflush(_LogFile);
    }
}

void Logger::setModuleName(const char *mname)
{
    size_t len = strlen(mname);

    size_t ctr = 0;

    int pos1 = 0;
    int pos2 = 0;

    pos1 = strcharacterpos(mname, '/');
    pos2 = strcharacterpos(mname, '\\');

    if(pos1 > -1 || pos2 > -1)
    {
        for(ctr = len; ; ctr--)
        {
            if(mname[ctr] == '/' || mname[ctr] == '\\')
            {
                break;
            }
        }
        char buffer[32]={0};

        strncpy((char*)&buffer[0], (char*)&mname[ctr+1], 32);

        _ModuleName = buffer;
    }
    else
    {
        _ModuleName = mname;
    }

    strreplace(_ModuleName, ".exe", "");
    strreplace(_ModuleName, ".EXE", "");
}

void Logger::setLogFileSize(int flsz)
{
    _LogFileSize = flsz;
}

void Logger::setLogDirectory(std::string dirpath)
{
    _LogDirectory = dirpath;

    char buffer[2048]={0};

    strcpy(buffer, _LogDirectory.c_str());

    if(buffer[strlen(buffer)-1]== '/' || buffer[strlen(buffer)-1]== '\\')
    {
        buffer[strlen(buffer)-1] = 0;
    }

    strcat(buffer, ".bak/");

    _LogBackupDirectory = buffer;

    if(!dirisdirectory(buffer))
    {
        dircreatedirectory(buffer);
    }
}

void Logger::writeExtended(LogLevel llevel, const char *func, const char *file, int line, const char* format,...)
{
    char tempbuf[1024];
    memset((char*)&tempbuf[0],0,1024);
    va_list args;
    va_start(args, format);
    vsprintf(tempbuf, format, args);
    tempbuf[1023]=0;
    write(tempbuf,llevel,func,file,line);
    va_end(args);
}

std::string getDateString(const char *format)
{
    struct tm timeinfo;
    time_t rawtime;
    time(&rawtime);
    timeinfo = *localtime(&rawtime);

    string str = format;
    size_t pos = 0;
    bool ap = false;

    char buffer[256];
    memset((char*)&buffer[0],0,256);

    pos = str.find("ss");
    if(pos!= string::npos)
    {
        strreplace(str,"ss","%S");
    }

    pos = str.find("mm");
    if(pos!= string::npos)
    {
        strreplace(str,"mm","%M");
    }

    pos = str.find("hh");
    if(pos!= string::npos)
    {
        strreplace(str,"hh","%H");
    }
    else
    {   pos = str.find("h");
        if(pos!= string::npos)
        {
            strreplace(str,"h","%I");
            ap = true;
        }
    }

    pos = str.find("dd");
    if(pos!= string::npos)
    {
       strreplace(str,"dd","%d");
    }

    pos = str.find("MMMM");
    if(pos!= string::npos)
    {
       strreplace(str,"MMMM","%B");
    }
    else
    {
        pos = str.find("MM");
        if(pos!= string::npos)
        {
           strreplace(str,"MM","%m");
        }
    }

    pos = str.find("yyyy");
    if(pos!= string::npos)
    {
       strreplace(str,"yyyy","%Y");
    }
    else
    {
        pos = str.find("yy");
        if(pos!= string::npos)
        {
           strreplace(str,"yy","%y");
        }
    }

    if(ap)
    {
        str += "%p";

    }

    if(timeinfo.tm_year < 100)
    {
        timeinfo.tm_year += 100;
    }

    strftime(buffer,256,str.c_str(),&timeinfo);

    return buffer;
}

bool dirisdirectory(const std::string& str)
{
    DIR* dirp;

    dirp = opendir(str.c_str());
    if (dirp == nullptr)
    {
        closedir(dirp);
        return false;
    }
    closedir(dirp);
    return true;
}

void dircurrentdirectory(std::string& str)
{
    char filepathbuffer[1024];
    memset((char*)& filepathbuffer[0], 0, 1024);
    getcwd(&filepathbuffer[0], 1024);

    for (int ctr = 0; filepathbuffer[ctr] != '\0'; ctr++)
    {
        if (filepathbuffer[ctr] == '\\')
        {
            filepathbuffer[ctr] = '/';
        }
    }

    str = filepathbuffer;
}

void dirgetname(const std::string& fullpath, std::string& str)
{
    size_t i = 0;
    str = "";
    size_t len = fullpath.length();

    if (len < 1)
        return;

    for (i = len - 1; ; i--)
    {
        if (fullpath[i] == '\\' || fullpath[i] == '/')
        {
            str = &fullpath.c_str()[i + 1];
            break;
        }
    }

    return;
}

void dircreatedirectory(const std::string& str)
{
    mkdir(str.c_str(), S_IRWXU);
}

void dirgetparentdirectory(const std::string& str, std::string& pstr)
{
    size_t origlen = str.length();

    char* ptr = new char[origlen + 1];
    memset(ptr, 0, origlen + 1);
    memcpy(ptr, str.c_str(), origlen);

    size_t len = strlen(ptr);

    if (len < 2)
    {
        delete [] ptr;
        return;
    }

    size_t ctr = len - 1;

    while (true)
    {
        ptr[ctr] = 0;
        ctr--;
        if (ptr[ctr] == '/' || ptr[ctr] == '\\')
        {
            break;
        }
    }

    pstr = ptr;

    delete[] ptr;
}
