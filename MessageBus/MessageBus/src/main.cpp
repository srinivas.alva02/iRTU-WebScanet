// HTTPServer.cpp : Defines the entry point for the console application.
//

#include "Logger.hpp"
#include "MessageBroker.hpp"

int main(int argc, char* argv[])
{

    MessageBroker brokerservice(argv[0]);

    if (!brokerservice.initialize("49151"))
	{
		return -1;
	}

	brokerservice.run();

	return 0;
}
