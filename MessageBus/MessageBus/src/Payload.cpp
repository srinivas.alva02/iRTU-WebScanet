#include "Payload.hpp"
#include "StringEx.hpp"
#include <stdlib.h>
#include <memory.h>
#include <string.h>
#include <algorithm>

Payload::Payload()
{
    _RequestBuffer.erase();
    _Content = nullptr;
    _HasContent = false;
    _Request.erase();
    _URL.erase();
    _Protocol.erase();
    _Version.erase();
    _ResponseText.erase();
    _ResponseCode = -1;
    _MessageType = -1;
    _KeyList.clear();
    _ValueList.clear();
    _ContentSize = 0;
}

Payload::Payload(const char* buffer)
{
    _RequestBuffer.erase();
    _Content = nullptr;
    _HasContent = false;
    _Request.erase();
    _URL.erase();
    _Protocol.erase();
    _Version.erase();
    _ResponseText.erase();
    _ResponseCode = -1;
    _MessageType = -1;
    _RequestBuffer = buffer;
    _KeyList.clear();
    _ValueList.clear();
    _ContentSize = 0;
}

void Payload::reset()
{
}

Payload::~Payload()
{

    if(	_Content != nullptr)
    {
        delete _Content;
        _Content = nullptr;
    }

    _RequestBuffer.erase();
    _HasContent = false;
    _Request.erase();
    _URL.erase();
    _Protocol.erase();
    _Version.erase();
    _ResponseText.erase();
    _ResponseCode = -1;
    _MessageType = -1;
    _KeyList.clear();
    _ValueList.clear();
    _ContentSize = 0;
}

void Payload::setHeader(const char* buffer)
{
	reset();
    _RequestBuffer = buffer;
}

void Payload::attachBody(const char* buffer)
{
    if(	_Content != nullptr)
    {
        delete _Content;
        _Content = nullptr;
    }

	_ContentSize = strlen(buffer);

    _Content = new char[_ContentSize+1];
	memset(_Content, 0, _ContentSize+1);
    memcpy(_Content,buffer,_ContentSize);

	char temp[10] = { 0 };
	sprintf(temp, "%d", _ContentSize);

	addHeader("Content-Length", temp);
}

const char*	Payload::getRequest()
{
    return _Request.c_str();
}

const char*	Payload::getProtocol()
{
    return _Protocol.c_str();
}

const char*	Payload::getURL()
{
    return _URL.c_str();
}

const char*	Payload::getVersion()
{
    return _Version.c_str();
}

const char*	Payload::getResponseText()
{
    return _ResponseText.c_str();
}

long Payload::getResponseCode()
{
    return _ResponseCode;
}

long Payload::getMessageType()
{
    return _MessageType;
}

const char*	Payload::getContent()
{
    return _Content;
}

size_t Payload::getContentSize()
{
    return _ContentSize;
}


void Payload::getFieldValue(const char* fieldName, std::string &value)
{
    std::vector<std::string>::iterator iter = std::find(_KeyList.begin(), _KeyList.end(), fieldName);

    if(iter == _KeyList.end())
	{
		value = "";
	}
    else
    {
        size_t position = std::distance( _KeyList.begin(), iter) ;
        value = _ValueList.at(position);
    }
	return;
}


bool Payload::deSerialize()
{
	std::string fieldValueParams;
	std::string field, value;

    getLine(_MessageLine);
    decodeMessageIdentificationLine(_MessageLine.c_str());

	while(true)
	{
		getLine(fieldValueParams);
		processLine(fieldValueParams.c_str(), field, value);
		if(field.length() < 1)
		{
			break;
		}

        _KeyList.push_back(field);
        _ValueList.push_back(value);

		if(strcmp(field.c_str(),"Content-Length") == 0 || strcmp(field.c_str(), "content-length") == 0)
		{
			if(atoi(value.c_str()) > 0)
			{
                _HasContent = true;
                _ContentSize = atol(value.c_str());
			}
			else
			{
                _HasContent = false;
			}
			break;;
		}
	}
	return true;
}

bool Payload::hasBody()
{
    return _HasContent;
}

void Payload::getLine(std::string &line)
{
    if(_RequestBuffer.length()<1)
    {
        line.clear();
        return;
    }

	std::string next;

	std::string delim = "\r\n";

    int pos = strsplit(_RequestBuffer, delim, line, next);

    if(pos == -1)
    {
        line = _RequestBuffer;
        _RequestBuffer.clear();
        return;
    }

    _RequestBuffer = next;
}

void Payload::processLine(const char *line, std::string &field, std::string &value)
{
    int delimeterpos = strcharacterpos(line, ':');

	field = "";
	value = "";
	int ctr = 0;
	for(ctr = 0; line[ctr] != 0; ctr++)
	{
        if(ctr < delimeterpos)
		{
			field += line[ctr];
		}

        if(ctr > delimeterpos+1)
		{
			value += line[ctr];
		}
        if(ctr == delimeterpos)
		{
			continue;
		}
	}
}

void Payload::decodeMessageIdentificationLine(const char* requestLine)
{
    _Request.erase();
    _URL.erase();
    _Version.erase();
    _Protocol.erase();
    _ResponseCode = -1;
    _ResponseText.erase();
    _MessageType = REQUEST;

	int ws = 0;
	std::string token1, token2, token3;

	for(int index = 0; requestLine[index] != 0 ; index++)
	{
		if(requestLine[index] == ' ' || requestLine[index] == '\t')
		{
			ws++;
			continue;
		}
		if(ws > 2)
		{
			break;
		}

		if(ws == 0)
		{
			token1 += requestLine[index];
		}
		if(ws == 1)
		{
			token2 += requestLine[index];
		}
		if(ws == 2)
		{
			token3 += requestLine[index];
		}
	}

    if(strcharacterpos(token1.c_str(),'/') == -1)
	{
        _Request = token1;
        _URL = token2;
        strsplit(token3.c_str(),'/',_Protocol,_Version);
        _MessageType = REQUEST;
		return;
	}
	else
	{
        strsplit(token1.c_str(),'/',_Protocol,_Version);
        _ResponseCode = atoi(token2.c_str());
        _ResponseText = token3;
        _MessageType = RESPONSE;
		return;
	}
}

void Payload::encodeMessageIdentificationLine()
{
	char tempBuffer[1024];
	memset(tempBuffer,0,1024);
    if(_MessageType == RESPONSE)
	{
        sprintf(tempBuffer,"%s/%s %ld %s\r\n",_Protocol.c_str(),_Version.c_str(), _ResponseCode, _ResponseText.c_str());
	}
	else
	{
        sprintf(tempBuffer,"%s %s %s/%s\r\n",_Request.c_str(),_URL.c_str(),_Protocol.c_str(),_Version.c_str());
	}
    _MessageLine = tempBuffer;
}

void Payload::setProtocolInformation(const char* request, const char* URL, const char* protocol, const char* version)
{
    _MessageType = REQUEST;
    _KeyList.clear();
    _ValueList.clear();
    _Protocol = protocol;
    _Version = version;
    _Request = request;
    _URL = URL;
}

void Payload::setProtocolInformation(const char* protocol, const char* version, long responsecode, const char* responsetext)
{
    _MessageType = RESPONSE;
    _KeyList.clear();
    _ValueList.clear();
    _Protocol = protocol;
    _Version = version;
    _ResponseCode = responsecode;
    _ResponseText = responsetext;
}

void Payload::addHeader(const char* field, const char* value)
{
	if (std::find(_KeyList.begin(), _KeyList.end(), field) != _KeyList.end())
	{
		return;
	}

    _KeyList.push_back(field);
    _ValueList.push_back(value);
}

std::string Payload::getHeader(std::string headerName)
{
    size_t ctr = 0;

	for (auto s : _KeyList)
	{
		if (s == headerName)
		{
			return _ValueList[ctr];
		}
		ctr++;
	}

    return "";
}


void Payload::serialize(std::string &sipString)
{
	encodeMessageIdentificationLine();

    char buffer[8096]={0};
    char temp[1025]={0};

    strcpy(buffer,_MessageLine.c_str());

    size_t headercount = _KeyList.size();

    for(size_t index = 0; index < headercount; index++)
    {
        sprintf(temp,"%s: %s\r\n",((std::string)_KeyList[index]).c_str(),((std::string)_ValueList[index]).c_str());
        strcat(buffer,temp);
    }

    strcat(buffer,"\r\n");

	sipString = buffer;
}


