#include "MessageBroker.hpp"
#include "Logger.hpp"
#include "StringEx.hpp"
#include "Responder.hpp"
#include "Payload.hpp"

#include <thread>
#include <memory.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <unistd.h>
#include <thread>
#include <mutex>
#include <iostream>

#define INVALID_SOCKET (-1)
#define SOCKET_ERROR	 (-1)
#define LPSOCKADDR sockaddr*

MessageBroker* broker = nullptr;
void RunResponder(Responder* responderPtr);
void handleProtocol(Payload* message, Responder *responderPtr);
bool receivePayload(Payload &message, Responder*responderPtr);
bool sendPayload(Payload &message, Responder*responderPtr);

static socklen_t addrlen;
static std::map<std::string,  Responder*> clientNodes;
static std::mutex socket_mutex;

BrokerSignalHandler::BrokerSignalHandler()
{

}

void BrokerSignalHandler::suspend()
{
	writeLog("SUSPEND SIGNAL", LOG_CRITICAL);
}

void BrokerSignalHandler::resume()
{
	writeLog("RESUME SIGNAL", LOG_CRITICAL);
}

void BrokerSignalHandler::shutdown()
{
	writeLog("SHUTDOWN SIGNAL", LOG_CRITICAL);
	exit(0);
}

void BrokerSignalHandler::alarm()
{
	writeLog("ALARM SIGNAL", LOG_CRITICAL);
}

void BrokerSignalHandler::reset()
{
	writeLog("RESET SIGNAL", LOG_CRITICAL);
}

void BrokerSignalHandler::childExit()
{
	writeLog("CHILD PROCESS EXIT SIGNAL", LOG_CRITICAL);
}

void BrokerSignalHandler::userdefined1()
{
	writeLog("USER DEFINED 1 SIGNAL", LOG_CRITICAL);
}

void BrokerSignalHandler::userdefined2()
{
	writeLog("USER DEFINED 2 SIGNAL", LOG_CRITICAL);
}

MessageBroker::MessageBroker(std::string appname)
{
    Logger::GetInstance()->setModuleName(appname.c_str());
	Logger::GetInstance()->startLogging();	
	broker = this;
}

MessageBroker::~MessageBroker()
{
}

bool MessageBroker::initialize(std::string port)
{
    _Port = atoi(port.c_str());

	_SigHdlr.registerCallbackClient(&_AppSignals);
	_SigHdlr.registerSignalHandlers();

	return true;
}

RunState MessageBroker::run()
{
    _ListenerSocket = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);

    sockaddr_in bindAddr;

    memset(static_cast<void*>(&bindAddr), 0, sizeof(sockaddr_in));

    bindAddr.sin_family = AF_INET;
    bindAddr.sin_port = htons(_Port);
    bindAddr.sin_addr.s_addr = htonl(INADDR_ANY);

    if(bind(_ListenerSocket,(sockaddr*)&bindAddr,sizeof(bindAddr)) == SOCKET_ERROR)
    {
        return BindFailed;
    }

    if(listen(_ListenerSocket,5)==SOCKET_ERROR)
    {
        return ListenFailed;
    }

    while(true)
    {
        sockaddr remotehostaddr;
        memset((void*)&remotehostaddr, 0, sizeof(remotehostaddr));
        addrlen = sizeof(remotehostaddr);

        int sock = accept(_ListenerSocket,&remotehostaddr,&addrlen);
        if(sock != INVALID_SOCKET)
        {
            Responder* client_responder = new Responder();
            client_responder->createSocket(sock);
            std::thread responderthread(RunResponder, client_responder);
            responderthread.detach();
        }
        else
        {
            if ((errno != ECHILD) && (errno != ERESTART) && (errno != EINTR))
            {
                return SignalReceived;
            }
        }
    }
}

void MessageBroker::stop()
{
    close(_ListenerSocket);
}

void RunResponder(Responder *responderPtr)
{
    while(true)
    {
        Payload client_payload;

        if(receivePayload(client_payload, responderPtr))
        {
            handleProtocol(&client_payload, responderPtr);
        }
        else
        {
            break;
        }
    }

    responderPtr->closeSocket();

    socket_mutex.lock();

    std::map<std::string,  Responder*>::iterator it;

    std::string sender;

    for (it = clientNodes.begin(); it != clientNodes.end(); ++it)
    {
        if(it->second == responderPtr)
        {
            sender = it->first;
            break;
        }
    }

    clientNodes.erase(it);

    delete responderPtr;

    for ( auto node : clientNodes )
    {
        Payload server_payload;
        std::string data;
        server_payload.setProtocolInformation("NODEOFFLINE", "/", "MESSAGEBUS", "1.0.0");
        server_payload.addHeader("Sender", "MessageBus");
        server_payload.addHeader("Recipient", node.first.c_str());
        server_payload.addHeader("Node", sender.c_str());

        server_payload.serialize(data);
        size_t len = data.length();

        node.second->sendBuffer(data.c_str(), len);
    }

    socket_mutex.unlock();

    return;
}

void handleProtocol(Payload* message, Responder *responderPtr)
{
    std::string sender = message->getHeader("Sender");
    std::string recipient = message->getHeader("Recipient");

    Payload server_payload;
    std::string data;

    if(strcmp((const char*)message->getRequest(),"REGISTER")==0)
    {
        socket_mutex.lock();
        if(clientNodes.find(sender) == clientNodes.end())
        {
            clientNodes.insert(std::pair<std::string, Responder*>(sender, responderPtr));
        }
        socket_mutex.unlock();

        // Send node online to all except the new one

        std::string node_list;

        socket_mutex.lock();
        for ( auto node : clientNodes )
        {
            if(node.first == sender)
            {
                continue;
            }

            server_payload.setProtocolInformation("NODEONLINE", "/", "MESSAGEBUS", "1.0.0");
            server_payload.addHeader("Sender", "MessageBus");
            server_payload.addHeader("Recipient", node.first.c_str());
            server_payload.addHeader("Node", sender.c_str());

            server_payload.serialize(data);
            size_t len = data.length();

            node.second->sendBuffer(data.c_str(), len);
            node_list += node.first + ",";
        }
        socket_mutex.unlock();

        if(node_list.length() > 1) node_list.pop_back();

        // Send the node list to the new node

        if(node_list.length() > 0)
        {
            char content_length[8] = {0};
            sprintf(content_length, "%d", node_list.length());

            Payload node_list_payload;
            data.clear();
            node_list_payload.setProtocolInformation("NODELIST", "/", "MESSAGEBUS", "1.0.0");
            node_list_payload.addHeader("Sender", "MessageBus");
            node_list_payload.addHeader("Recipient", sender.c_str());
            node_list_payload.addHeader("Content-Length", content_length);
            node_list_payload.attachBody(node_list.c_str());

            node_list_payload.serialize(data);
            size_t len = 0;
            len = data.length();
            responderPtr->sendBuffer(data.c_str(), len);
            len = node_list.length();
            responderPtr->sendBuffer(node_list.c_str(), len);
        }

        return;
    }

    if(strcmp((const char*)message->getRequest(),"DEREGISTER")==0)
    {
        socket_mutex.lock();
        if(clientNodes.find(sender) != clientNodes.end())
        {
            clientNodes.erase(sender);
        }
        socket_mutex.unlock();

        responderPtr->closeSocket();

        // Send node offline to all

        socket_mutex.lock();
        for ( auto node : clientNodes )
        {
            server_payload.setProtocolInformation("NODEOFFLINE", "/", "MESSAGEBUS", "1.0.0");
            server_payload.addHeader("Sender", "MessageBus");
            server_payload.addHeader("Recipient", node.first.c_str());
            server_payload.addHeader("Node", sender.c_str());

            server_payload.serialize(data);
            size_t len = data.length();

            node.second->sendBuffer(data.c_str(), len);
        }
        socket_mutex.unlock();

        return;
    }

    Responder* destination = nullptr;

    socket_mutex.lock();
    if(clientNodes.find(recipient) != clientNodes.end())
    {
        destination = clientNodes.find(recipient)->second;

        if(strcmp((const char*)message->getRequest(),"DATA")==0 ||
                strcmp((const char*)message->getRequest(),"EVENT")==0 ||
                strcmp((const char*)message->getRequest(),"REQUEST")==0 ||
                strcmp((const char*)message->getRequest(),"RESPONSE")==0)
        {
            std::string data;
            size_t len = 0;

            message->serialize(data);
            len = data.length();
            destination->sendBuffer(data.c_str(), len);

            len = message->getContentSize();
            destination->sendBuffer(message->getContent(), len);
        }
    }
    socket_mutex.unlock();
}

bool sendPayload(Payload &message, Responder*responderPtr)
{
    if(!responderPtr->isConnected())
    {
        return false;
    }

    std::string data;
    message.serialize(data);
    size_t len = data.length();
    return responderPtr->sendBuffer(data.c_str(), len);
}

bool receivePayload(Payload &message, Responder*responderPtr)
{
    if(!responderPtr->isConnected())
    {
        return false;
    }

    std::string header;
    if(responderPtr->receiveString(header,(char*)"\r\n\r\n"))
    {
        message.setHeader(header.c_str());
        message.deSerialize();

        if(message.hasBody())
        {
            char *buffer = nullptr;

            int len = message.getContentSize();

            buffer = new char[len+1];
            memset(buffer,0,len+1);

            if(responderPtr->receiveBuffer(buffer,len))
            {
                message.attachBody(buffer);
                delete [] buffer;
                buffer = nullptr;
            }
            else
            {
                return false;
            }
        }
    }
    else
    {
        return false;
    }

    return true;
}
